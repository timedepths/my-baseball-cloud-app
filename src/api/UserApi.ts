import axios from 'axios';
import { httpClient } from '.';
import {
  IClientData,
  IUploadAvatarProps,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from '../store/user/types';

export async function SignIn({ email, password }: IUserAuthorizationProps) {
  try {
    const clientData = httpClient
      .post('auth/sign_in', {
        email: email,
        password: password,
      })
      .then(response => {
        return {
          accessToken: response.headers['access-token'],
          client: response.headers['client'],
          uid: response.headers['uid'],
        } as IClientData;
      });
    return clientData;
  } catch (error) {
    return {
      accessToken: '',
      client: '',
      uid: '',
    } as IClientData;
  }
}

export async function SignUp({
  email,
  password,
  confirmPassword,
  role,
}: IUserRegistrationProps) {
  try {
    const clientData = httpClient
      .post('auth', {
        email: email,
        password: password,
        confirm_password: confirmPassword,
        role: role,
      })
      .then(response => {
        return {
          accessToken: response.headers['access-token'],
          client: response.headers['client'],
          uid: response.headers['uid'],
        } as IClientData;
      });
    return clientData;
  } catch (error) {
    return {
      accessToken: '',
      client: '',
      uid: '',
    } as IClientData;
  }
}

export async function SignOut() {
  await httpClient.delete('auth/sign_out');
}

export async function fetchUserData({ accessToken, client, uid }: IClientData) {
  const user = httpClient
    .post(
      'graphql',
      {
        query: `{ 
        current_profile () {
          id
          first_name
          last_name
          position
          position2
          avatar
          throws_hand
          bats_hand
          biography
          school_year
          feet
          inches
          weight
          age
          school {
            id
            name
          }  
          teams {
            id
            name
          }  
          facilities {
            id
            email
            u_name
          }
        }
      }`,
      },
      {
        headers: {
          'Access-Token': accessToken,
          Client: client,
          Uid: uid,
        },
      },
    )
    .then(response => response.data);
  return user;
}

export async function UploadAvatar({
  fileName,
  accessToken,
  client,
  uid,
}: IUploadAvatarProps) {
  try {
    const avatarUrl = httpClient
      .post(
        's3/signed_url',
        {
          name: fileName,
        },
        {
          headers: {
            'access-Token': accessToken,
            client: client,
            uid: uid,
          },
        },
      )
      .then(response => {
        const tempHttpClient = axios.create();
        tempHttpClient.interceptors.request.use(
          config => {
            config.headers = {
              'access-Token': accessToken,
              client: client,
              uid: uid,
            };
            return config;
          },
          error => Promise.reject(error),
        );
        tempHttpClient.put(response.data.signedUrl);
        return `https://baseballcloud-staging-assets.s3.us-east-2.amazonaws.com/${response.data.fileKey}`;
      });
    return avatarUrl;
  } catch {
    return '';
  }
}
