import axios from 'axios';

export const httpClient = axios.create({
  baseURL: 'https://baseballcloud-back.herokuapp.com/api/v1/',
});
