import * as React from 'react';

function CheckMark(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={14}
      height={15}
      viewBox="0 0 14 15"
      {...props}>
      <path
        fill="#FFF"
        fillRule="evenodd"
        d="M6.116 10.884l5.482-5.482a.566.566 0 000-.804l-.91-.91a.566.566 0 00-.804 0l-4.17 4.17L3.83 5.972a.566.566 0 00-.803 0l-.91.91a.566.566 0 000 .804l3.196 3.197c.223.223.58.223.803 0zM13.714 3v8.571a2.572 2.572 0 01-2.571 2.572H2.57A2.572 2.572 0 010 11.57V3A2.572 2.572 0 012.571.429h8.572A2.572 2.572 0 0113.714 3z"
      />
    </svg>
  );
}

export default CheckMark;
