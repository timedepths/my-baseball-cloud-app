import styled from 'styled-components';
import { InputProps } from './InputProps';

const ShortInput = ({ type, placeHolder, renderProps }: InputProps) => {
  const { input } = renderProps;
  return (
    <InputWrapper>
      <StyledInput {...input} type={type} placeholder={placeHolder} />
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  width: 35%;
  padding: 0px 16px;
  border-radius: 4px;
  background-color: #eff1f3;
`;
const StyledInput = styled.input`
  width: 100%;
  outline: none;
  height: 38px;
  background-color: #eff1f3;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  border: 1px solid transparent;
`;

export default ShortInput;
