import { useState } from 'react';
import styled from 'styled-components';

type ItemData = {
  value: string;
  optionValue: string;
};

type ComboBoxProps = {
  itemsList: Array<ItemData>;
  onChangeItem: (optionValue: string) => void;
};

const ComboBox = ({ itemsList, onChangeItem }: ComboBoxProps) => {
  const [isShowingList, setShowingListState] = useState(false);
  const [selectedItemValue, setSelectedItemValue] = useState(
    itemsList[0].value,
  );
  const [selectedItemIndex, setSelectedItemIndex] = useState(0);

  const list = itemsList.map((value, index) => (
    <ListItem
      key={index}
      isSelected={selectedItemIndex === index}
      onClick={() => {
        setShowingListState(false);
        setSelectedItemValue(value.value);
        setSelectedItemIndex(index);
        onChangeItem(value.optionValue);
      }}>
      <ListItemText>{value.value}</ListItemText>
    </ListItem>
  ));

  return (
    <ComboBoxContainer>
      <ComboBoxHeader onClick={() => setShowingListState(!isShowingList)}>
        <ComboBoxHeaderText>{selectedItemValue}</ComboBoxHeaderText>
        <SelectArrowSpan>
          {isShowingList ? (
            <span className="fa fa-angle-up"></span>
          ) : (
            <span className="fa fa-angle-down"></span>
          )}
        </SelectArrowSpan>
      </ComboBoxHeader>
      {isShowingList && <ItemsList>{list}</ItemsList>}
    </ComboBoxContainer>
  );
};

const ComboBoxContainer = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
`;
const ComboBoxHeader = styled.div`
  width: 100%;
  height: 38px;
  border-radius: 4px;
  background-color: #eff1f3;
  line-height: 1.13;
  font-weight: 400;
  color: #667784;
  border: 1px solid transparent;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const ComboBoxHeaderText = styled.div`
  padding: 0px 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const ItemsList = styled.div`
  position: absolute;
  top: 47px;
  width: 100%;
  height: 150px;
  overflow-y: scroll;
  border-radius: 5px;
  box-shadow: 0px 0px 5px rgba(61, 61, 61, 0.2);
`;
const ListItem = styled.div<{ isSelected: boolean }>`
  width: 100%;
  height: 38px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  display: flex;
  align-items: center;
  color: #667784;
  background-color: ${props => (!props.isSelected ? '#fff' : '#def')};
`;
const ListItemText = styled.div`
  padding: 0px 16px;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
`;
const SelectArrowSpan = styled.span`
  width: 27px;
  float: right;
  text-align: left;
  padding-top: 2px;
`;

export default ComboBox;
