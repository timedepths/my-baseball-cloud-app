import styled from 'styled-components';
import ChooseAvatarForm from './ChooseAvatarForm';
import UserEditorForm from './UserEditorForm';

const ProfileEditorPanel = () => {
  return (
    <MainContainer>
      <ChooseAvatarForm />
      <UserEditorForm />
    </MainContainer>
  );
};

const MainContainer = styled.aside`
  background: #fff;
  border-left: 1px solid rgba(0, 0, 0, 0.1);
  width: 264px;
  overflow-x: auto;
  overflow-y: scroll;
  padding: 16px;
`;

export default ProfileEditorPanel;
