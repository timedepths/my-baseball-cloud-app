import { FieldRenderProps } from 'react-final-form';

export type InputProps = {
  type: string;
  placeHolder: string;
  renderProps: FieldRenderProps<string>;
};
