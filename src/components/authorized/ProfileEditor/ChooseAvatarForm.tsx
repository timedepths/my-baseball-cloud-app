import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { uploadAvatarAction } from '../../../store/user/actions';
import {
  selectAvatarLoadingStatus,
  selectClientData,
  selectUserData,
} from '../../../store/user/slice';

const ChooseAvatarForm = () => {
  const { avatar } = useSelector(selectUserData);
  const { accessToken, client, uid } = useSelector(selectClientData);
  const avatarIsUploading = useSelector(selectAvatarLoadingStatus);
  const [fileName, setFileName] = useState('');
  const [filePath, setFilePath] = useState('');
  const dispatch = useDispatch();

  return (
    <AvatarContainer>
      <Avatar imageUrl={!!filePath ? filePath : avatar} />
      <input
        style={{ display: 'none' }}
        id="uploadAvatar"
        type="file"
        onChange={e => {
          const list = e.currentTarget.files;
          if (list) {
            let reader = new FileReader();
            reader.onloadend = () => {
              if (reader.result) {
                setFileName(list[0].name);
                setFilePath(reader.result.toString());
              }
            };
            reader.readAsDataURL(list[0]);
          }
        }}
      />
      {!avatarIsUploading ? (
        !!filePath ? (
          <BottomContainer>
            <FileNameText>{!!fileName ? fileName : 'Unknown'}</FileNameText>
            <ButtonsContainer>
              <div>
                <UploadButton
                  onClick={() => {
                    dispatch(
                      uploadAvatarAction({
                        fileName,
                        accessToken,
                        client,
                        uid,
                      }),
                    );
                    setFileName('');
                    setFilePath('');
                  }}>
                  Upload Photo
                </UploadButton>
                <CancelButton
                  onClick={() => {
                    setFileName('');
                    setFilePath('');
                  }}>
                  Cancel
                </CancelButton>
              </div>
            </ButtonsContainer>
          </BottomContainer>
        ) : (
          <StyledLabel htmlFor="uploadAvatar">Choose Photo</StyledLabel>
        )
      ) : (
        <span>Loading...</span>
      )}
    </AvatarContainer>
  );
};

const AvatarContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 23px;
`;
const Avatar = styled.div<{ imageUrl: string }>`
  width: 100px;
  height: 100px;
  background-image: url(${props => props.imageUrl});
  background-size: cover;
  background-position: 50% 50%;
  border-radius: 50%;
  margin-bottom: 8px;
`;
const StyledLabel = styled.label`
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  font-weight: 400;
  color: #788b99;
  line-height: 1;
  cursor: pointer;
  white-space: nowrap;
  margin-top: 8px;
  :hover {
    text-decoration: underline;
    color: #33adff;
  }
`;
const FileNameText = styled.span`
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  font-weight: 400;
  margin-bottom: 13px;
  color: #788b99;
  line-height: 1;
  cursor: pointer;
  white-space: nowrap;
`;
const BottomContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 6px;
`;
const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  flex: 0 0 100%;
  align-items: center;
  justify-content: center;
`;
const UploadButton = styled.a`
  font-family: 'Lato', sans-serif;
  margin-right: 20px;
  font-size: 16px;
  line-height: 1.13;
  font-weight: 400;
  color: #48bbff;
  cursor: pointer;
`;
const CancelButton = styled.a`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  line-height: 1.13;
  font-weight: 400;
  color: #788b99;
  cursor: pointer;
`;

export default ChooseAvatarForm;
