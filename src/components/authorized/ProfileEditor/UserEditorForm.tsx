import { Form, Field } from 'react-final-form';
import styled from 'styled-components';
import ShortInput from './ShortInput';
import ComboBox from './ComboBox';
import { IUser } from '../../../store/user/types';
import { useState } from 'react';

const positions = [
  { value: 'Catcher', optionValue: 'catcher' },
  { value: 'First Base', optionValue: 'first_base' },
  { value: 'Second Base', optionValue: 'second_base' },
  { value: 'Shortstop', optionValue: 'shortstop' },
  { value: 'Third Base', optionValue: 'third_base' },
  { value: 'Outfield', optionValue: 'outfield' },
  { value: 'Pitcher', optionValue: 'pitcher' },
];

const UserEditorForm = () => {
  const [firstPositionValue, setFirstPositionValue] = useState(positions[0].optionValue);

  const handleUpdateSubmit = (values: IUser) => {
    alert(
      values.firstName + ' / ' + values.lastName + ' / ' + firstPositionValue,
    );
  };
  return (
    <Form
      onSubmit={handleUpdateSubmit}
      render={({ handleSubmit }) => (
        <FormContainer onSubmit={handleSubmit}>
          <LineForInitials>
            <Field name="firstName">
              {props => (
                <ShortInput
                  type="text"
                  placeHolder="First Name *"
                  renderProps={props}
                />
              )}
            </Field>
            <Field name="lastName">
              {props => (
                <ShortInput
                  type="text"
                  placeHolder="Last Name *"
                  renderProps={props}
                />
              )}
            </Field>
          </LineForInitials>
          <ComboBox
            itemsList={positions}
            onChangeItem={optionValue => setFirstPositionValue(optionValue)}
          />
          <button type="submit">Save</button>
        </FormContainer>
      )}
    />
  );
};

const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
`;
const LineForInitials = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

export default UserEditorForm;
