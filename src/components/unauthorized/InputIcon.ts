import styled from 'styled-components';

export const InputIcon = styled.span`
  position: absolute;
  left: 19px;
  top: 19px;
  color: #667784;
`;
