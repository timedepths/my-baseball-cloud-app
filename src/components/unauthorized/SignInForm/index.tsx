import { Field, Form } from 'react-final-form';
import styled from 'styled-components';
import Input from '../Input';
import { Button } from '../Button';
import SignInValidator from '../../../utils/SignInValidator';
import { useState } from 'react';
import { InputIcon } from '../InputIcon';
import { useDispatch } from 'react-redux';
import { signInAction } from '../../../store/user/actions';

type SignInProps = {
  email: string;
  password: string;
};

const SignInForm = () => {
  const [isSubmitted, setSubmitState] = useState(false);
  const dispatch = useDispatch();

  const handleSignInSubmit = ({ email, password }: SignInProps) => {
    dispatch(signInAction({ email, password }));
  };
  return (
    <SignInContainer>
      <SignInFormHeader>
        <SignInFormHeaderTopText>
          Welcome to BaseballCloud!
        </SignInFormHeaderTopText>
        <SignInFormHeaderBottomText>
          Sign into your account here:
        </SignInFormHeaderBottomText>
      </SignInFormHeader>
      <Form
        onSubmit={handleSignInSubmit}
        validate={SignInValidator}
        render={({ handleSubmit, errors, submitting }) => (
          <form onSubmit={handleSubmit}>
            <FieldWrapper>
              <InputIcon className="fa fa-user" />
              <Field name="email">
                {props => (
                  <Input type="email" placeHolder="Email" renderProps={props} />
                )}
              </Field>
            </FieldWrapper>
            <FieldWrapper>
              <InputIcon className="fa fa-lock" />
              <Field name="password">
                {props => (
                  <Input
                    type="password"
                    placeHolder="Password"
                    renderProps={props}
                  />
                )}
              </Field>
            </FieldWrapper>
            {isSubmitted && !!errors?.message && (
              <ErrorContainer>{errors?.message}</ErrorContainer>
            )}
            <Button
              type="submit"
              onClick={() => setSubmitState(true)}
              disabled={submitting}>
              Sign In
            </Button>
            <FirstLinkWrapper>
              <StyledFirstLink href="/password">
                Forgotten password?
              </StyledFirstLink>
            </FirstLinkWrapper>
          </form>
        )}
      />
      <SignInFormFooter>
        <SignInFormFooterText>Don’t have an account?</SignInFormFooterText>
        <StyledSecondLink href="/registration">Sign Up</StyledSecondLink>
      </SignInFormFooter>
    </SignInContainer>
  );
};

const SignInContainer = styled.div`
  background: hsla(0, 0%, 100%, 0.8);
  padding: 16px;
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  box-shadow: 0 0 20px rgb(0 0 0 / 40%);
  backdrop-filter: blur(5px);
  max-width: 418px;
  width: 100%;
`;
const SignInFormHeader = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 48px;
`;
const SignInFormHeaderTopText = styled.div`
  font-size: 24px;
  font-family: 'Lato', sans-serif;
  line-height: 1.25;
  text-align: center;
  color: #667784;
  margin-bottom: 8px;
`;
const SignInFormHeaderBottomText = styled.div`
  line-height: 1.25;
  font-family: 'Lato', sans-serif;
  text-align: center;
  color: #667784;
  font-size: 16px;
`;
const FieldWrapper = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 15px;
`;
const ErrorContainer = styled.div`
  font-family: 'Lato', sans-serif;
  display: flex;
  margin-top: 8px;
  color: #f05f62;
`;
const FirstLinkWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 15px;
`;
const StyledFirstLink = styled.a`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #337ab7;
  text-decoration: none;
  :hover {
    text-decoration: underline;
  }
`;
const SignInFormFooter = styled.div`
  display: flex;
  justify-content: center;
`;
const SignInFormFooterText = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const StyledSecondLink = styled.a`
  font-family: 'Lato', sans-serif;
  padding-left: 3px;
  line-height: 1.13;
  color: #48bbff;
  text-decoration: underline;
  font-size: 16px;
`;

export default SignInForm;
