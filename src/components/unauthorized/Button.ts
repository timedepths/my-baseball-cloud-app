import styled from 'styled-components';

export const Button = styled.button`
  color: #fff;
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  border: solid 1px transparent;
  background-color: #48bbff;
  padding-top: 15px;
  padding-bottom: 17px;
  width: 100%;
  flex: 1 1 auto;
  margin-bottom: 15px;
  cursor: pointer;
  :hover {
    box-shadow: 0 0 4px 0 rgb(72 187 255 / 80%);
  }
`;
