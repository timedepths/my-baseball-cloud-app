import { Field, Form } from 'react-final-form';
import styled from 'styled-components';
import Input from '../Input';
import { Button } from '../Button';
import ForgotPasswordValidator from '../../../utils/ForgotPasswordValidator';
import { useState } from 'react';
import { InputIcon } from '../InputIcon';

type ForgotPasswordProps = {
  email: string;
};

const ForgotPasswordForm = () => {
  const [isSubmitted, setSubmitState] = useState(false);

  const handleSubmit = ({ email }: ForgotPasswordProps) => {};
  return (
    <Container>
      <FormHeader>
        <FormHeaderTopText>Forgot Password</FormHeaderTopText>
        <FormHeaderBottomText>
          Please enter your email address. You will receive a link to reset your
          password via email.
        </FormHeaderBottomText>
      </FormHeader>
      <Form
        onSubmit={handleSubmit}
        validate={ForgotPasswordValidator}
        render={({ handleSubmit, errors, submitting }) => (
          <form onSubmit={handleSubmit}>
            <FieldWrapper>
              <InputIcon className="fa fa-user" />
              <Field name="email">
                {props => (
                  <Input type="email" placeHolder="Email" renderProps={props} />
                )}
              </Field>
            </FieldWrapper>
            {isSubmitted && !!errors?.message && (
              <ErrorContainer>Required</ErrorContainer>
            )}
            <Button
              type="submit"
              onClick={() => setSubmitState(true)}
              disabled={submitting}>
              Submit
            </Button>
          </form>
        )}
      />
      <FormFooter>
        <FormFooterText>Remember password?</FormFooterText>
        <StyledSecondLink href="/login">Sign In</StyledSecondLink>
      </FormFooter>
    </Container>
  );
};

const Container = styled.div`
  background: hsla(0, 0%, 100%, 0.8);
  padding: 16px;
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  box-shadow: 0 0 20px rgb(0 0 0 / 40%);
  backdrop-filter: blur(5px);
  max-width: 418px;
  width: 100%;
`;
const FormHeader = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 48px;
`;
const FormHeaderTopText = styled.div`
  font-size: 24px;
  font-family: 'Lato', sans-serif;
  line-height: 1.25;
  text-align: center;
  color: #667784;
  margin-bottom: 8px;
`;
const FormHeaderBottomText = styled.div`
  line-height: 1.25;
  font-family: 'Lato', sans-serif;
  text-align: center;
  color: #667784;
  font-size: 16px;
`;
const FieldWrapper = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 15px;
`;
const ErrorContainer = styled.div`
  font-family: 'Lato', sans-serif;
  display: flex;
  margin-top: 8px;
  color: #f05f62;
`;
const FormFooter = styled.div`
  display: flex;
  justify-content: center;
`;
const FormFooterText = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const StyledSecondLink = styled.a`
  font-family: 'Lato', sans-serif;
  padding-left: 3px;
  line-height: 1.13;
  color: #48bbff;
  text-decoration: underline;
  font-size: 16px;
`;

export default ForgotPasswordForm;
