import { FieldRenderProps } from 'react-final-form';
import styled from 'styled-components';

type InputProps = {
  type: string;
  placeHolder: string;
  renderProps: FieldRenderProps<string>;
};

const Input = ({ type, placeHolder, renderProps }: InputProps) => {
  const { input } = renderProps;
  return <StyledInput {...input} type={type} placeholder={placeHolder} />;
};
const StyledInput = styled.input`
  font-family: 'Lato', sans-serif;
  outline: none;
  width: 100%;
  height: 50px;
  border-radius: 4px;
  background-color: #eff1f3;
  padding: 0px 12px 0px 37px;
  font-size: 16px;
  line-height: 1.13;
  color: #667784;
  border: 1px solid transparent;
  transition: background-color, color;
  transition-duration: 0.5s;

  :focus {
    border: 1px solid #48bbff;
    background-color: #fff;
  }
`;

export default Input;
