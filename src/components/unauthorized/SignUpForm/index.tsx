import { useState } from 'react';
import { Field, Form } from 'react-final-form';
import styled from 'styled-components';
import Input from '../Input';
import { CheckMark } from '../../../assets/svg';
import { Button } from '../Button';
import SignUpValidator from '../../../utils/SignUpValidator';
import { InputIcon } from '../InputIcon';
import { useDispatch } from 'react-redux';
import { signUpAction } from '../../../store/user/actions';

type SignUpProps = {
  email: string;
  password: string;
  confirmPassword: string;
};

const SignUpForm = () => {
  const [selectedRole, setRole] = useState('player');
  const [isSubmitted, setSubmitState] = useState(false);
  const dispatch = useDispatch();

  const handleSignUpSubmit = ({
    email,
    password,
    confirmPassword,
  }: SignUpProps) => {
    setSubmitState(true);
    dispatch(
      signUpAction({
        email,
        password,
        confirmPassword,
        role: selectedRole,
      }),
    );
  };
  return (
    <SignUpContainer>
      <SignUpFormHeader>
        {selectedRole === 'player' ? (
          <SignUpFormHeaderSelectedLeftButton>
            <div className="check-mark">
              <CheckMark />
            </div>
            Sign Up as Player
          </SignUpFormHeaderSelectedLeftButton>
        ) : (
          <SignUpFormHeaderLeftButton onClick={() => setRole('player')}>
            Sign Up as Player
          </SignUpFormHeaderLeftButton>
        )}
        {selectedRole === 'scout' ? (
          <SignUpFormHeaderSelectedRightButton>
            <div className="check-mark">
              <CheckMark />
            </div>
            Sign Up as Scout
          </SignUpFormHeaderSelectedRightButton>
        ) : (
          <SignUpFormHeaderRightButton onClick={() => setRole('scout')}>
            Sign Up as Scout
          </SignUpFormHeaderRightButton>
        )}
      </SignUpFormHeader>
      <InfoContainer>
        {selectedRole === 'player' && (
          <>
            <InfoHeader>Players</InfoHeader>
            <InfoBody>
              Players have their own profile within the system and plan on
              having data collected.
            </InfoBody>
          </>
        )}
        {selectedRole === 'scout' && (
          <>
            <InfoHeader>Scouts</InfoHeader>
            <InfoBody>
              Coaches and scouts can view players in the system but do not have
              their own profile.
            </InfoBody>
          </>
        )}
      </InfoContainer>
      <Form
        onSubmit={handleSignUpSubmit}
        validate={SignUpValidator}
        render={({ handleSubmit, errors }) => {
          return (
            <form onSubmit={handleSubmit}>
              <FieldWrapper>
                <InputIcon className="fa fa-user" />
                <Field name="email">
                  {props => (
                    <Input
                      type="email"
                      placeHolder="Email"
                      renderProps={props}
                    />
                  )}
                </Field>
              </FieldWrapper>
              {isSubmitted && !!errors?.email && (
                <ErrorContainer>{errors?.email}</ErrorContainer>
              )}
              <FieldWrapper>
                <InputIcon className="fa fa-lock" />
                <Field name="password">
                  {props => (
                    <Input
                      type="password"
                      placeHolder="Password"
                      renderProps={props}
                    />
                  )}
                </Field>
              </FieldWrapper>
              {isSubmitted && !!errors?.password && (
                <ErrorContainer>{errors?.password}</ErrorContainer>
              )}
              <FieldWrapper>
                <InputIcon className="fa fa-check" />
                <Field name="confirmPassword">
                  {props => (
                    <Input
                      type="password"
                      placeHolder="Confirm Password"
                      renderProps={props}
                    />
                  )}
                </Field>
              </FieldWrapper>
              {isSubmitted && !!errors?.confirm && (
                <ErrorContainer>{errors?.confirm}</ErrorContainer>
              )}
              <LegalContainer>
                By clicking Sign Up, you agree to our{' '}
                <LegalContainerLink href="/legal/terms">
                  Terms of Service
                </LegalContainerLink>{' '}
                and{' '}
                <LegalContainerLink href="/legal/privacy">
                  Privacy Policy
                </LegalContainerLink>
                .
              </LegalContainer>
              <Button type="submit" onClick={() => setSubmitState(true)}>
                Sign Up
              </Button>
            </form>
          );
        }}
      />
      <SignUpFormFooter>
        <SignUpFormFooterText>Already registered?</SignUpFormFooterText>
        <StyledSecondLink href="/login">Sign In</StyledSecondLink>
      </SignUpFormFooter>
    </SignUpContainer>
  );
};

const SignUpContainer = styled.div`
  background: hsla(0, 0%, 100%, 0.8);
  padding: 16px;
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  box-shadow: 0 0 20px rgb(0 0 0 / 40%);
  backdrop-filter: blur(5px);
  max-width: 418px;
  width: 100%;
`;
const SignUpFormHeader = styled.div`
  display: flex;
  margin-bottom: 20px;
`;
const SignUpFormHeaderLeftButton = styled.button`
  cursor: pointer;
  padding: 15px 5px 17px;
  border-radius: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  color: #35c32a;
  background-color: #ffffff;
  display: flex;
  flex: 1 1 auto;
  border: solid 1px #35c32a;
  justify-content: center;
  font-size: 16px;
  font-family: 'Lato', sans-serif;
  font-weight: 700;
  line-height: 1.13;
  :hover {
    background-color: #35c32a;
    color: #ffffff;
  }
`;
const SignUpFormHeaderRightButton = styled.button`
  cursor: pointer;
  padding: 15px 5px 17px;
  border-radius: 0;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  color: #35c32a;
  background-color: #ffffff;
  display: flex;
  flex: 1 1 auto;
  border: solid 1px #35c32a;
  justify-content: center;
  font-size: 16px;
  font-family: 'Lato', sans-serif;
  font-weight: 700;
  line-height: 1.13;
  :hover {
    background-color: #35c32a;
    color: #ffffff;
  }
`;
const SignUpFormHeaderSelectedLeftButton = styled.button`
  cursor: pointer;
  padding: 15px 5px 17px;
  border-radius: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  background-color: #35c32a;
  color: #ffffff;
  display: flex;
  flex: 1 1 auto;
  border: solid 1px #35c32a;
  justify-content: center;
  font-size: 16px;
  font-family: 'Lato', sans-serif;
  font-weight: 700;
  line-height: 1.13;
`;
const SignUpFormHeaderSelectedRightButton = styled.button`
  cursor: pointer;
  padding: 15px 5px 17px;
  border-radius: 0;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  background-color: #35c32a;
  color: #ffffff;
  display: flex;
  flex: 1 1 auto;
  border: solid 1px #35c32a;
  justify-content: center;
  font-size: 16px;
  font-family: 'Lato', sans-serif;
  font-weight: 700;
  line-height: 1.13;
`;
const InfoContainer = styled.div`
  background: #48bbff;
  border-radius: 8px;
  padding: 16px;
  margin-bottom: 16px;
`;
const InfoHeader = styled.div`
  font-size: 36px;
  text-align: center;
  font-weight: 700;
  line-height: 0.78;
  color: #ffffff;
  margin-bottom: 21px;
  font-family: 'Lato', sans-serif;
`;
const InfoBody = styled.p`
  margin: 0;
  line-height: 1.44;
  text-align: center;
  color: #ffffff;
  font-size: 0.88rem;
  font-family: 'Lato', sans-serif;
`;
const FieldWrapper = styled.div`
  position: relative;
  display: flex;
  position: relative;
  margin-bottom: 15px;
`;
const ErrorContainer = styled.div`
  font-family: 'Lato', sans-serif;
  display: flex;
  margin-top: 4px;
  margin-bottom: 15px;
  color: #f05f62;
`;
const LegalContainer = styled.div`
  font-family: 'Lato', sans-serif;
  display: block;
  margin-bottom: 8px;
  margin-top: 12px;
  padding-left: 10px;
  padding-right: 10px;
  line-height: 1.42857143;
  color: #333;
`;
const LegalContainerLink = styled.a`
  color: #337ab7;
  text-decoration: none;
  :hover {
    text-decoration: underline;
  }
`;
const SignUpFormFooter = styled.div`
  display: flex;
  justify-content: center;
`;
const SignUpFormFooterText = styled.div`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  color: #667784;
`;
const StyledSecondLink = styled.a`
  font-family: 'Lato', sans-serif;
  padding-left: 3px;
  line-height: 1.13;
  color: #48bbff;
  text-decoration: underline;
  font-size: 16px;
`;

export default SignUpForm;
