import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { MainLogo } from '../../assets/svg';

type NavBarProps = {
  isAuthorized: boolean;
};

const NavBar = ({ isAuthorized }: NavBarProps) => {
  if (isAuthorized) {
    return (
      <NavBarForAuthorized>
        <Link to="/profile">
          <MainLogo />
        </Link>
        <NavBarContentBody>
          <StyledLink href="/leaderboard">
            Leaderboard
            <LinkUnderLine />
          </StyledLink>
          <StyledLink href="/network">
            Network
            <LinkUnderLine />
          </StyledLink>
        </NavBarContentBody>
      </NavBarForAuthorized>
    );
  } else {
    return (
      <NavBarContainer>
        <Link to="/login">
          <MainLogo />
        </Link>
      </NavBarContainer>
    );
  }
};

const NavBarForAuthorized = styled.header`
  z-index: 10;
  width: 100%;
  height: 39px;
  padding: 8px;
  padding-bottom: 5px;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  grid-area: hd;
  border-bottom: 1px solid rgba(120, 139, 153, 0.5);
`;
const NavBarContainer = styled.header`
  z-index: 10;
  width: 100%;
  padding: 8px;
  padding-bottom: 5px;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  grid-area: hd;
`;
const NavBarContentBody = styled.div`
  display: flex;
  flex-direction: row;
  grid-area: hd;
`;
const StyledLink = styled.a`
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  padding: 0 8px;
  color: #788b99;
  text-decoration: none;
  position: relative;
`;
const LinkUnderLine = styled.div`
  position: absolute;
  top: 12px;
  opacity: 0.5;
  width: 85%;
  height: 100%;
  :hover {
    border-bottom: 3px solid #788b99;
  }
`;

export default NavBar;
