import styled from 'styled-components';

const Footer = () => {
  return (
    <FooterContainer>
      <div className="legal">
        <span>© 2018 BaseballCloud</span>
        <a href="/legal/terms">Terms of Service</a>
        <a href="/legal/privacy">Privacy Policy</a>
      </div>
      <div className="socialMedia">
        <a href="https://baseballcloud.blog">Blog</a>
        <a href="http://twitter.com/baseballcloudus">Twitter</a>
        <a href="http://www.instagram.com/baseballcloudus/">Instagram</a>
        <a href="http://www.facebook.com/BaseballCloudUS/">Facebook</a>
      </div>
    </FooterContainer>
  );
};

const FooterContainer = styled.footer`
  z-index: 10;
  width: 100%;
  bottom: 0;
  padding-left: 5px;
  color: #333;
  grid-area: ft;
  background: #fff;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export default Footer;
