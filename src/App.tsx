import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { AuthorizedPages, UnauthorizedPages } from './pages';
import { NavBar, Footer } from './components';
import { useSelector } from 'react-redux';
import { selectClientData } from './store/user/slice';
import styled from 'styled-components';

function App() {
  const clientData = useSelector(selectClientData);
  const isAuthorized =
    !!clientData.accessToken && !!clientData.client && !!clientData.uid;

  return (
    <BrowserRouter>
      <MainContainer>
        <NavBar isAuthorized={isAuthorized} />
        <Switch>
          {isAuthorized &&
            AuthorizedPages.map(value => (
              <Route key={Math.round(Math.random() * 100)} exact path={value.path} component={value.component} />
            ))}
          {!isAuthorized &&
            UnauthorizedPages.map(value => (
              <Route key={Math.round(Math.random() * 100)} exact path={value.path} component={value.component} />
            ))}
          {isAuthorized ? <Redirect to="/profile" /> : <Redirect to="/login" />}
        </Switch>
        <Footer />
      </MainContainer>
    </BrowserRouter>
  );
}

const MainContainer = styled.div`
  height: 100%;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: auto 1fr auto;
  grid-template-areas:
    'hd'
    'content'
    'ft';
  box-sizing: border-box;
`;

export default App;
