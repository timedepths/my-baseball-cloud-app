type SignUpProps = {
  email: string;
  password: string;
  confirmPassword: string;
};

const SignUpValidator = ({ email, password, confirmPassword }: SignUpProps) => {
  const error = { email: '', password: '', confirm: '' };
  if (!email) {
    error.email = 'Required';
  } else if (!/\w+@\w+[.]\w+/.test(email)) {
    error.email = 'Invalid email address';
  }
  if (!password) {
    error.password = 'Required';
  } else if (password.length < 8) {
    error.password = 'Must contain more than 8 characters';
  } else if (password !== confirmPassword) {
    error.confirm = 'Passwords are not equal';
  }
  if (error.email || error.password || error.confirm) {
    return error;
  }
  return undefined;
};
export default SignUpValidator;
