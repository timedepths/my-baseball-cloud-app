type SignInProps = {
  email: string;
  password: string;
};

const SignInValidator = ({ email, password }: SignInProps) => {
  const error = { message: '' };
  if (
    !(email && password && password.length >= 8 && /\w+@\w+[.]\w+/.test(email))
  ) {
    error.message = 'Invalid login credentials. Please Try again.';
    return error;
  }
  return undefined;
};

export default SignInValidator;
