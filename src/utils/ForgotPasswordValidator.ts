type ForgotPasswordProps = {
  email: string;
};

const ForgotPasswordValidator = ({ email }: ForgotPasswordProps) => {
  const error = { message: '' };
  if (!(email && /\w+@\w+[.]\w+/.test(email))) {
    error.message = 'Required';
    return error;
  }
  return undefined;
};
export default ForgotPasswordValidator;
