import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchUserDataAction } from '../../store/user/actions';
import { selectClientData, selectUserData } from '../../store/user/slice';

import { ProfileInfoPanel, ProfileEditorPanel } from '../../components';

const ProfilePage = () => {
  const userData = useSelector(selectUserData);
  const clientData = useSelector(selectClientData);
  const dispatch = useDispatch();

  const [isEditProfile, setEditProfileState] = useState(false);

  useEffect(() => {
    if (userData.id === -1) {
      dispatch(fetchUserDataAction({ ...clientData }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  return (
    <PageContainer>
      <InsideContainer>
        {isEditProfile ? (
          <ProfileEditorPanel />
        ) : (
          <ProfileInfoPanel onEditProfile={() => setEditProfileState(true)} />
        )}
      </InsideContainer>
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  box-sizing: border-box;
`;

const InsideContainer = styled.div`
  display: flex;
  flex: 2;
  overflow: auto;
  width: 100%;
  height: 100%;
`;

export default ProfilePage;
