import ForgotPasswordPage from './unauthorized/ForgotPasswordPage';
import SignInPage from './unauthorized/SignInPage';
import SignUpPage from './unauthorized/SignUpPage';
import ProfilePage from './authorized/ProfilePage';

export const UnauthorizedPages = [
  {
    path: '/login',
    component: SignInPage,
  },
  {
    path: '/registration',
    component: SignUpPage,
  },
  {
    path: '/password',
    component: ForgotPasswordPage,
  },
];

export const AuthorizedPages = [
  {
    path: '/profile',
    component: ProfilePage,
  },
];
