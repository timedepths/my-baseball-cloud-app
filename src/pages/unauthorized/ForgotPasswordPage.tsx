import React from 'react';
import styled from 'styled-components';
import { ForgotPasswordForm } from '../../components';

const ForgotPasswordPage = () => {
  return (
    <PageContainer>
      <InsideContainer>
        <ForgotPasswordForm />
      </InsideContainer>
    </PageContainer>
  );
};

const PageContainer = styled.div`
  grid-area: content;
  background: #fff;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  box-sizing: border-box;
`;

const InsideContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: auto;
  padding: 16px;
  background-image: url(./images/unauthorizedBackground.png);
  background-position: top center;
  background-size: cover;
  box-sizing: border-box;
`;

export default ForgotPasswordPage;
