import { IFacility } from '../facilities/types';
import { ISchool } from '../schools/types';
import { ITeam } from '../teams/types';

export interface IUserAuthorizationProps {
  email: string;
  password: string;
}
export interface IUserRegistrationProps extends IUserAuthorizationProps {
  confirmPassword: string;
  role: string;
}

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  avatar: string;
  firstPosition: string;
  secondPosition: string;
  school: ISchool;
  schoolYear: string;
  biography: string;
  teams: Array<ITeam>;
  facilities: Array<IFacility>;
  weight: number;
  feet: number;
  inches: number;
  age: number;
  batsHand: string;
  throwsHand: string;
}

export interface IClientData {
  accessToken: string;
  client: string;
  uid: string;
}
export interface IUploadAvatarProps extends IClientData {
  fileName: string;
}
