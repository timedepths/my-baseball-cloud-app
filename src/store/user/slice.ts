import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';
import { IClientData, IUser } from './types';

const initialState = {
  isUploadAvatar: false,
  clientData: {
    accessToken: '',
    client: '',
    uid: '',
  } as IClientData,
  data: {
    id: -1,
    avatar: '',
    firstName: '',
    lastName: '',
    firstPosition: '',
    secondPosition: '',
    school: { id: 0, name: '' },
    schoolYear: '',
    biography: '',
    teams: [],
    facilities: [],
    weight: 0,
    feet: 0,
    inches: 0,
    age: 0,
    batsHand: '',
    throwsHand: '',
  } as IUser,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    initializeAccessData: (state, action: PayloadAction<IClientData>) => {
      state.clientData = action.payload;
    },
    initializeUserData: (state, action: PayloadAction<IUser>) => {
      state.data = action.payload;
    },
    setUploadAvatarState: (state, action: PayloadAction<boolean>) => {
      state.isUploadAvatar = action.payload;
    },
    uploadPhoto: (state, action: PayloadAction<string>) => {
      state.data.avatar = action.payload;
      state.isUploadAvatar = false;
    },
    signOut: state => {
      state.clientData = {
        accessToken: '',
        client: '',
        uid: '',
      };
      state.data = {
        id: -1,
        avatar: '',
        firstName: '',
        lastName: '',
        firstPosition: '',
        secondPosition: '',
        school: { id: 0, name: '' },
        schoolYear: '',
        biography: '',
        teams: [],
        facilities: [],
        weight: 0,
        feet: 0,
        inches: 0,
        age: 0,
        batsHand: '',
        throwsHand: '',
      };
    },
  },
});

export const userReducer = userSlice.reducer;
export const {
  initializeAccessData,
  initializeUserData,
  signOut,
  uploadPhoto,
  setUploadAvatarState,
} = userSlice.actions;

export const selectUserData = createSelector(
  (state: RootState) => state.user,
  data => data.data,
);
export const selectClientData = createSelector(
  (state: RootState) => state.user,
  data => data.clientData,
);
export const selectAvatarLoadingStatus = createSelector(
  (state: RootState) => state.user,
  data => data.isUploadAvatar,
);
