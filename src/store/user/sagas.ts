import { put, call, StrictEffect, takeEvery } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import * as UserApi from '../../api/UserApi';
import {
  initializeAccessData,
  initializeUserData,
  setUploadAvatarState,
  signOut,
  uploadPhoto,
} from './slice';
import {
  fetchUserDataAction,
  signInAction,
  signOutAction,
  signUpAction,
  uploadAvatarAction,
} from './actions';
import {
  IClientData,
  IUploadAvatarProps,
  IUser,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from './types';

function* signInSaga(
  action: PayloadAction<IUserAuthorizationProps>,
): Generator<StrictEffect, void, IClientData> {
  const clientData = yield call(UserApi.SignIn, action.payload);
  yield put(initializeAccessData({ ...clientData }));
}
export function* watchSignIn() {
  yield takeEvery(signInAction, signInSaga);
}

function* signUpSaga(
  action: PayloadAction<IUserRegistrationProps>,
): Generator<StrictEffect, void, IClientData> {
  const clientData = yield call(UserApi.SignUp, action.payload);
  yield put(initializeAccessData({ ...clientData }));
}
export function* watchSignUp() {
  yield takeEvery(signUpAction, signUpSaga);
}

function* signOutSaga(): Generator<StrictEffect, void, void> {
  yield call(UserApi.SignOut);
  yield put(signOut);
}
export function* watchSignOut() {
  yield takeEvery(signOutAction, signOutSaga);
}

function* fetchUserDataSaga(
  action: PayloadAction<IClientData>,
): Generator<StrictEffect, void, any> {
  const data = yield call(UserApi.fetchUserData, { ...action.payload });
  const currentProfile = { ...data.data.current_profile };

  let firstPosition = '';
  switch (currentProfile.position) {
    case 'first_base':
      firstPosition = 'First Base';
      break;
    case 'second_base':
      firstPosition = 'Second Base';
      break;
    case 'third_base':
      firstPosition = 'Third Base';
      break;
    case 'catcher':
      firstPosition = 'Catcher';
      break;
    case 'pitcher':
      firstPosition = 'Pitcher';
      break;
    case 'outfield':
      firstPosition = 'Outfield';
      break;
    case 'shortstop':
      firstPosition = 'Shortstop';
      break;
  }

  let secondPosition = '';
  switch (currentProfile.position2) {
    case 'first_base':
      secondPosition = 'First Base';
      break;
    case 'second_base':
      secondPosition = 'Second Base';
      break;
    case 'third_base':
      secondPosition = 'Third Base';
      break;
    case 'catcher':
      secondPosition = 'Catcher';
      break;
    case 'pitcher':
      secondPosition = 'Pitcher';
      break;
    case 'outfield':
      secondPosition = 'Outfield';
      break;
    case 'shortstop':
      secondPosition = 'Shortstop';
      break;
    case '':
      secondPosition = '';
      break;
  }

  let schoolYear = '';
  switch (currentProfile.school_year) {
    case 'freshman':
      schoolYear = 'Freshman';
      break;
    case 'sophomore':
      schoolYear = 'Sophomore';
      break;
    case 'senior':
      schoolYear = 'Senior';
      break;
    case 'junior':
      schoolYear = 'Junior';
      break;
    case 'none':
      schoolYear = 'None';
      break;
  }

  const user = {
    id: currentProfile.id,
    firstName: currentProfile.first_name,
    lastName: currentProfile.last_name,
    avatar: currentProfile.avatar,
    age: currentProfile.age,
    batsHand: currentProfile.bats_hand,
    throwsHand: currentProfile.throws_hand,
    teams: currentProfile.teams,
    facilities: currentProfile.facilities,
    school: currentProfile.school,
    schoolYear: schoolYear,
    feet: currentProfile.feet,
    inches: currentProfile.inches,
    weight: currentProfile.weight,
    firstPosition: firstPosition,
    secondPosition: secondPosition,
    biography: currentProfile.biography,
  } as IUser;
  yield put(initializeUserData({ ...user }));
}
export function* watchFetchUserData() {
  yield takeEvery(fetchUserDataAction, fetchUserDataSaga);
}

function* uploadAvatarSaga(
  action: PayloadAction<IUploadAvatarProps>,
): Generator<StrictEffect, void, string> {
  yield put(setUploadAvatarState(true));
  const imageUrl = yield call(UserApi.UploadAvatar, action.payload);
  yield put(uploadPhoto(imageUrl));
}
export function* watchUploadAvatar() {
  yield takeEvery(uploadAvatarAction, uploadAvatarSaga);
}
