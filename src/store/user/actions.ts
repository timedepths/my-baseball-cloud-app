import { createAction } from '@reduxjs/toolkit';
import {
  IClientData,
  IUploadAvatarProps,
  IUserAuthorizationProps,
  IUserRegistrationProps,
} from './types';

export const signInAction =
  createAction<IUserAuthorizationProps>('user/SIGN_IN');
export const signUpAction =
  createAction<IUserRegistrationProps>('user/SIGN_UP');
export const signOutAction = createAction('user/SIGN_OUT');
export const fetchUserDataAction = createAction<IClientData>('user/FETCH_DATA');
export const uploadAvatarAction = createAction<IUploadAvatarProps>('user/UPLOAD_IMAGE');
