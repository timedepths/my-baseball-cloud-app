import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';
import {
  watchFetchUserData,
  watchSignIn,
  watchSignOut,
  watchSignUp,
  watchUploadAvatar,
} from './user/sagas';
import { userReducer } from './user/slice';

const rootReducer = combineReducers({
  user: userReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const persistConfig = {
  key: 'localData',
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

function* rootSaga() {
  yield all([
    // user
    watchSignUp(),
    watchSignIn(),
    watchSignOut(),
    watchFetchUserData(),
    watchUploadAvatar(),
  ]);
}

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: persistedReducer,
  middleware: [...getDefaultMiddleware(), sagaMiddleware],
});
sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
